var currentDisplayerPlayer1 = 0;
var currentDisplayerPlayer2 = 0;
var playerOptions = ['Blue', 'Gray', 'Green', 'Orange', 'Pink', 'Red', 'Yellow'];
var gameState = 'playerSelect';
var player1Score = 0;
var player2Score = 0;
var currentTrack = 0;
var trackOptions = ['alien', 'triangle', 'diamond', 'loop', 'maze']
var player1GoalLastHit = 'none';
var playrt2GoalLastHit = 'none';

window.onload = function () {
	game = new Phaser.Game(800, 600, Phaser.CANVAS, 'phaser-example', {
		preload: preload,
		create: create,
		update: update,
		render: render
	});
}

function preload() {
	game.load.image('playerSelectScreen', 'assets/screens/playerSelectScreen.png');

	game.load.image('loopTrack', 'assets/tracks/loop.png');
	game.load.physics('loopPhysics', 'Assets/tracks/loopPhysics.json');

	game.load.image('mazeTrack', 'assets/tracks/maze.png');
    game.load.physics('mazePhysics', 'Assets/tracks/mazePhysics.json');

    game.load.image('diamondTrack', 'assets/tracks/diamond.png');
    game.load.physics('diamondPhysics', 'Assets/tracks/diamondPhysics.json');

    game.load.image('alienTrack', 'assets/tracks/alien.png');
    game.load.physics('alienPhysics', 'Assets/tracks/alienPhysics.json');

    game.load.image('triangleTrack', 'assets/tracks/triangle.png');
    game.load.physics('trianglePhysics', 'Assets/tracks/trianglePhysics.json');

	game.load.image('rightArrow', 'assets/buttons/rightArrow.png');
	game.load.image('leftArrow', 'assets/buttons/leftArrow.png');
	game.load.image('playButton', 'assets/buttons/playButton.png');

	game.load.image('playerBlue', 'assets/players/playerBlue.png');
	game.load.image('playerGray', 'assets/players/playerGray.png');
	game.load.image('playerGreen', 'assets/players/playerGreen.png');
	game.load.image('playerOrange', 'assets/players/playerOrange.png');
	game.load.image('playerPink', 'assets/players/playerPink.png');
	game.load.image('playerRed', 'assets/players/playerRed.png');
    game.load.image('playerYellow', 'assets/players/playerYellow.png');

    game.load.image('player1Goal', 'assets/players/player1Goal.png');
    game.load.image('player2Goal', 'assets/players/player2Goal.png');

	game.stage.backgroundColor = '#b97a56';
}

function create() {
	game.physics.startSystem(Phaser.Physics.P2JS);

	game.physics.p2.gravity.y = 0;

	forwardButton = game.input.keyboard.addKey(Phaser.Keyboard.W);
	backwardButton = game.input.keyboard.addKey(Phaser.Keyboard.S);
	leftButton = game.input.keyboard.addKey(Phaser.Keyboard.A);
	rightButton = game.input.keyboard.addKey(Phaser.Keyboard.D);
	cursors = game.input.keyboard.createCursorKeys();

	playerSelectScreen = game.add.sprite(0, 0, 'playerSelectScreen');

	leftArrow1 = game.add.button(50, 300, 'leftArrow', scroll1Left, this);
	rightArrow1 = game.add.button(300, 300, 'rightArrow', scroll1Right, this);
	leftArrow2 = game.add.button(450, 300, 'leftArrow', scroll2Left, this);
	rightArrow2 = game.add.button(700, 300, 'rightArrow', scroll2Right, this);

	player1 = game.add.sprite(200, 325, 'playerBlue');
	player1.scale.setTo(0.1, 0.1);
	game.physics.p2.enable(player1);
	player1.scale.setTo(0.3, 0.3);
    player1.body.static = true;
    player1.body.bounce = 0.2;

	player2 = game.add.sprite(600, 325, 'playerBlue');
	player2.scale.setTo(0.1, 0.1);
	game.physics.p2.enable(player2);
	player2.scale.setTo(0.3, 0.3);
    player2.body.static = true;
    player2.body.bounce = 0.2;

	playButton = game.add.button(400, 500, 'playButton', playPressed, this);
	playButton.anchor.x = 0.5;
	playButton.scale.setTo(0.5, 0.5);
    playButton.alpha = 0.5;

    player1Goal = game.add.sprite(Math.random() * 600 + 100, Math.random() * 400 + 100, 'player1Goal');
    player1Goal.scale.setTo(0.5, 0.5);
    game.physics.p2.enable(player1Goal);
    player1Goal.visible = false;
    player1Goal.name = 'goal';
    player1Goal.body.onBeginContact.add(player1GoalHit, this);

    player2Goal = game.add.sprite(Math.random() * 600 + 100, Math.random() * 400 + 100, 'player2Goal');
    player2Goal.scale.setTo(0.5, 0.5);
    game.physics.p2.enable(player2Goal);
    player2Goal.visible = false;
    player2Goal.name = 'goal';
    player2Goal.body.onBeginContact.add(player2GoalHit, this);

    player1ScoreText = game.add.text(16, 16, 'Player1 score: 0', {
        font: '28pt arial',
        fill: '#0b0d2b'
    });

    player2ScoreText = game.add.text(475, 16, 'Player2 score: 0', {
        font: '28pt arial',
        fill: '#0b0d2b'
    });

    game.physics.p2.setPostBroadphaseCallback(checkGoalTrack, this);
}

function update() {
    player1ScoreText.text = 'Player1 score: ' + player1Score;
    player2ScoreText.text = 'Player2 score: ' + player2Score;

    player1Goal.body.setZeroVelocity();
    player2Goal.body.setZeroVelocity();

	if (gameState == 'racing') {
		if (navigator.getGamepads()[0] != null) {
			leftGamepadAxisY = navigator.getGamepads()[0].axes[1];
            leftGamepadAxisX = navigator.getGamepads()[0].axes[0];
            rightGamepadTrigger = navigator.getGamepads()[0].buttons[7].value;
            leftGamepadTrigger = navigator.getGamepads()[0].buttons[6].value;

            player2.body.setZeroRotation();
            if (leftGamepadAxisX > 0.2) {
                player2.body.rotateRight(100 * leftGamepadAxisX);
            } else if (leftGamepadAxisX < -0.2) {
                player2.body.rotateLeft(100 * -leftGamepadAxisX);
            }

            if (rightGamepadTrigger > 0.1) {
                player2.body.reverse(400 * rightGamepadTrigger);
            } else if (leftGamepadTrigger > 0.1) {
                player2.body.thrust(400 * leftGamepadTrigger);
            }
		}

		player1.body.setZeroRotation()
		if (rightButton.isDown) {
			player1.body.rotateRight(100);
		}
		if (leftButton.isDown) {
			player1.body.rotateLeft(100);
		}

		if (forwardButton.isDown) {
            player1.body.reverse(400);
		}
        if (backwardButton.isDown) {
            player1.body.thrust(400);
		}
    }

    if (checkOverlap(player1, player1Goal)) {
        player1Goal.reset(Math.random() * 600 + 100, Math.random() * 400 + 100);
        player1Score++;
        if (player1Score % 5 == 0) {
            currentTrack++;
            if (currentTrack >= trackOptions.length) {
                currentTrack = 0;
            }
            track.body.clearShapes();
            track.body.loadPolygon(trackOptions[currentTrack] + 'Physics', trackOptions[currentTrack]);
            track.loadTexture(trackOptions[currentTrack] + 'Track');
            player1.reset(32, 32);
            player2.reset(64, 98);
        }
    }
    if (checkOverlap(player2, player1Goal)) {
        player1Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
    }
    if (checkOverlap(player1, player2Goal)) {
        player2Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
    }
    if (checkOverlap(player2, player2Goal)) {
        player2Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
        player2Score++;
        if (player2Score % 5 == 0) {
            currentTrack++;
            if (currentTrack >= trackOptions.length) {
                currentTrack = 0;
            }
            track.body.clearShapes();
            track.body.loadPolygon(trackOptions[currentTrack] + 'Physics', trackOptions[currentTrack]);
            track.loadTexture(trackOptions[currentTrack] + 'Track');
            player1.reset(32, 32);
            player2.reset(64, 98);
        }
    }

    if (!player1Goal.inCamera || player1Goal.x == NaN || player1Goal.y == NaN) {
        player1Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
    }
    if (!player2Goal.inCamera) {
        player2Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
    }
}

function render() {
	game.debug.body(player1);
	game.debug.body(player2);
}

function scroll1Left() {
	currentDisplayerPlayer1--;
	if (currentDisplayerPlayer1 < 0) {
		currentDisplayerPlayer1 = playerOptions.length-1;
	}

	player1.loadTexture('player' + playerOptions[currentDisplayerPlayer1]);

	if (player1.key == player2.key) {
		game.add.tween(playButton).to({
			alpha: 0.5
		}, 75, Phaser.Easing.Linear.None, true);
	} else {
		game.add.tween(playButton).to({
			alpha: 1.0
		}, 75, Phaser.Easing.Linear.None, true);
	}
}

function scroll1Right() {
	currentDisplayerPlayer1++;
	if (currentDisplayerPlayer1 >= playerOptions.length) {
		currentDisplayerPlayer1 = 0;
	}

	player1.loadTexture('player' + playerOptions[currentDisplayerPlayer1]);

	if (player1.key == player2.key) {
		game.add.tween(playButton).to({
			alpha: 0.5
		}, 75, Phaser.Easing.Linear.None, true);
	} else {
		game.add.tween(playButton).to({
			alpha: 1.0
		}, 75, Phaser.Easing.Linear.None, true);
	}
}

function scroll2Left() {
	currentDisplayerPlayer2--;
	if (currentDisplayerPlayer2 < 0) {
		currentDisplayerPlayer2 = playerOptions.length - 1;
	}

	player2.loadTexture('player' + playerOptions[currentDisplayerPlayer2]);

	if (player1.key == player2.key) {
		game.add.tween(playButton).to({
			alpha: 0.5
		}, 75, Phaser.Easing.Linear.None, true);
	} else {
		game.add.tween(playButton).to({
			alpha: 1.0
		}, 75, Phaser.Easing.Linear.None, true);
	}
}

function scroll2Right() {
	currentDisplayerPlayer2++;
	if (currentDisplayerPlayer2 >= playerOptions.length) {
		currentDisplayerPlayer2 = 0;
	}

	player2.loadTexture('player' + playerOptions[currentDisplayerPlayer2]);

	if (player1.key == player2.key) {
		game.add.tween(playButton).to({
			alpha: 0.5
		}, 75, Phaser.Easing.Linear.None, true);
	} else {
		game.add.tween(playButton).to({
			alpha: 1.0
		}, 75, Phaser.Easing.Linear.None, true);
	}
}

function playPressed() {
	if (player1.key != player2.key) {
		gameState = 'racing';

		playerSelectScreen.visible = false;
		leftArrow1.visible = false;
		rightArrow1.visible = false;
		leftArrow2.visible = false;
		rightArrow2.visible = false;
		playButton.visible = false;

        track = game.add.sprite(game.world.centerX, game.world.centerY, trackOptions[0] + 'Track');
        game.physics.p2.enable(track);
        track.body.clearShapes();
        track.body.loadPolygon(trackOptions[0] + 'Physics', trackOptions[0]);
        track.body.static = true;
        track.name = 'track';

        player1Goal.visible = true;
        player2Goal.visible = true;

		game.add.tween(player1.scale).to({
			x: 0.1,
			y: 0.1
		}, 1000, Phaser.Easing.Linear.None, true);
		game.add.tween(player1.body).to({
			x: 32,
			y: 32
		}, 1000, Phaser.Easing.Linear.None, true);

		game.add.tween(player2.scale).to({
			x: 0.1,
			y: 0.1
		}, 1000, Phaser.Easing.Linear.None, true);
		game.add.tween(player2.body).to({
			x: 98,
			y: 64
		}, 1000, Phaser.Easing.Linear.None, true);

		game.time.events.add(250, function () {
			player1.body.static = false;
            player2.body.static = false;

            player1Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
            player2Goal.reset(Math.random() * 700 + 50, Math.random() * 500 + 50);
		}, 1100);
	}
}

function checkOverlap(spriteA, spriteB) {
    var boundsA = spriteA.getBounds();
    var boundsB = spriteB.getBounds();

    return Phaser.Rectangle.intersects(boundsA, boundsB);
}

function checkGoalTrack(body1, body2) {
    if (body1.sprite.name === 'goal' && body2.sprite.name === 'track') {
        body1.reset(Math.random() * 400 + 100, Math.random() * 600 + 100);
        return false;
    } else if (body2.sprite.name === 'goal' && body1.sprite.name === 'track') {
        body2.reset(Math.random() * 400 + 100, Math.random() * 600 + 100);
    }
    return true;
}

function player1GoalHit(body) {
    if (body) {
        player1GoalLastHit = body.sprite.key;
        console.log('1: ' + body.sprite.key);
    }
    else {
        player1GoalLastHit = 'bounds';
    }
}

function player2GoalHit(body) {
    if (body) {
        player2GoalLastHit = body.sprite.key;
        console.log('2: ' + body.sprite.key);
    }
    else {
        player2GoalLastHit = 'bounds';
    }
}